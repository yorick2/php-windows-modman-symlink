<?php
/**
* looks for symlink reference files which contain lines in the format 'source location' space then 'destination location'.
* Then links the destination location to the source location. It wil work for files and folders.
*
* Limitations:
* If you wish to run using a terminal window link Cmd or git bash this needs to be run as an administrator, 
* by right clicking ion the program and selecting 'Run as administrator'.
*
* If instead you are running using the browser Xampp or wamp and other PHP development environments have to started as the administrator.
* This is easiest achieved by uninstalling and then right clicking on the program and selecting "Run as administrator".
*
* example of a reference file contents
* src/app/code/community/PaulMillband/ManageCategoryPageLink app/code/community/PaulMillband/ManageCategoryPageLink
* src/app/design/adminhtml/default/default/layout/paulmillband_managecategorypagelink.xml app/design/adminhtml/default/default/layout/paulmillband_managecategorypagelink.xml
* src/app/etc/modules/PaulMillband_ManageCategoryPageLink.xml app/etc/modules/PaulMillband_ManageCategoryPageLink.xml
*/

class phpWindowsSymlinks {
    private $test=false;
    private $ReferenceFiles='modman';
    private $folderDepth=2; // levels down the folder tree from current location should look for reference file
    private $destination='../htdocs';// location symlinks need to go from. keep blank for same location as reference file
    private $searchStartFolder=''; // the starting position for the search for reference files

    /**
     * adjust variables that have defaults
     */
    function setDefaults(){
        if( strlen($this->searchStartFolder) === 0 ){
            $this->searchStartFolder = __DIR__;
        }else{
            $firstLetter = substring(trim($this->searchStartFolder),0,1)
            if( $firstLetter != '/' || $firstLetter != '\\')({
                $this->searchStartFolder = __DIR__.DIRECTORY_SEPARATOR.$this->searchStartFolder;
            }
        }
    }
    
    /**
     * change string to convert all slashes to the system directory seperator
     */
    function sanitizeFolderPaths($filePath){
        return str_replace(['/','\\'] , DIRECTORY_SEPARATOR, $filePath);
    }
    
    /**
     * find list of files matching the name $ReferenceFiles, in the folder 
     * $folderName recursively down to maximum folder depth of $maxFolderDepth
     * 
     * @param string $folderName
     * @param string $ReferenceFiles name of the files looking for
     * @param int $maxFolderDepth maximum folder levels deep should go
     * @param int $folderLevel current folder level
     * @return array List of files found  
     */
    function findFiles($folderName,$ReferenceFiles,$maxFolderDepth,$folderLevel=0)
    {
        $fileNamesArray = [];
        $dir = new DirectoryIterator($folderName);
        foreach ($dir as $fileInfo) {
            $filename = $fileInfo->getFileName();
            if (!$fileInfo->isDot() && $fileInfo->isDir() && $folderLevel < $maxFolderDepth ) {
                $moreFiles = $this->findFiles($folderName.DIRECTORY_SEPARATOR.$filename,$ReferenceFiles,$maxFolderDepth,$folderLevel+1);
                $fileNamesArray = array_merge($fileNamesArray,$moreFiles);
                $x=6;
            }elseif( $filename === $ReferenceFiles ){
                array_push($fileNamesArray, $folderName.DIRECTORY_SEPARATOR.$filename);
            }
        }
        return $fileNamesArray;
    }

    /**
     * get contents of given file
     * 
     * @param type $filePath
     * @return object|boolean file object or false
     */
    function getFile($filePath){
        if( file_exists($filePath) ){
            if( is_readable($filePath) ){
                return file($filePath);
            }else{
                error_log('cannot read file: '.$filePath);
                return false;
            }
        }else{
            error_log("file doesn't exist: ".$filePath);
            return false;
        }
    }

    /**
     * make array of links origins and destinations from file
     * 
     * @param array $fileNamesArray array of file paths to get links from 
     * @return array links origins and destinations from file
     */
    function makeLinksArray($fileNamesArray){
        $linkListArray = [];
        for( $i=0; $i<sizeof($fileNamesArray); $i++){
            $contents = $this->getFile($fileNamesArray[$i]);
            foreach($contents as $key => $rawLine) {
                $line = preg_split('/\s+/', trim($rawLine));
                if( isset($line[0]) && isset($line[1])){
                    array_push($linkListArray,['fileLocation'=>$fileNamesArray[$i],'file'=>$line[0],'link'=>$line[1]]);
                }elseif( sizeof ($contents[$key])>1 ){
                    $lineNo = $key + 1;
                    echo PHP_EOL."::ERROR:: error with the link reference file $fileNamesArray[$i] on line $lineNo. See php error log for details";
                    error_log("error with the link reference file $fileNamesArray[$i] on line $lineNo.\nline contents is\n$rawLine");
                }
            }
        }
        return $linkListArray;
    }

    /**
     * create the indevidual symlinks and parent folders if required
     * 
     * @param string $originalFile
     * @param string $link
     */
    function createSingleLinks($originalFile,$link){
        $parentFolder = dirname($link);
        if ( !file_exists($parentFolder) ) {
            mkdir($parentFolder, 0700, true);
        }
        if(!file_exists($link)){
//                $file = 'C:/xampp/htdocs/xxx/vendor/juno/landingpages/src/app/code/community/Juno/LandingPages';
//        $link = 'C:/xampp/htdocs/xxx/vendor/../htdocs/app/code/community/Juno/LandingPages';
            symlink( $originalFile,$link);    
        }
    }
    
    /**
     * create or print the links provided
     * 
     * @param array $linksArray array of links
     * @param string $destination root destination folder the links defined from 
     * @param boolean $test should it create the links
     */
    function createLinks($linksArray,$destination='',$test=true){
        echo "creating symlinks:";
        for($i=0; $i<sizeof($linksArray); $i++){
            $originalFile = dirname ($this->sanitizeFolderPaths($linksArray[$i]['fileLocation'])).DIRECTORY_SEPARATOR.$this->sanitizeFolderPaths($linksArray[$i]['file']);
            if($destination===''){
                $link = dirname ($this->sanitizeFolderPaths($linksArray[$i]['fileLocation'])).DIRECTORY_SEPARATOR.$this->sanitizeFolderPaths($linksArray[$i]['link']);
            }else{
                $link = $destination.DIRECTORY_SEPARATOR.$this->sanitizeFolderPaths($linksArray[$i]['link']);
            }
            echo PHP_EOL."$link $originalFile";
            if ($test === false){
                $this->createSingleLinks($originalFile,$link);
            }
        }
        if ($test === false){
            echo PHP_EOL."===============================================";
            echo PHP_EOL."links created";
            echo PHP_EOL."================================================";
        }else{
            echo PHP_EOL."===============================================";
            echo PHP_EOL."run with test as false to create links";
            echo PHP_EOL."================================================";
        }
    }

    /**
     * runs the script
     */
    function run(){
        $this->setDefaults();
        $this->destination =  $this->sanitizeFolderPaths($this->destination);
        $this->searchStartFolder =  $this->sanitizeFolderPaths($this->searchStartFolder);
        $fileNamesArray = $this->findFiles($this->searchStartFolder,$this->ReferenceFiles,$this->folderDepth);
        $linksArray = $this->makeLinksArray($fileNamesArray);
        $this->createLinks($linksArray,$this->destination,$this->test);            
    }


}

$phpWindowsSymlinks = new phpWindowsSymlinks;
$phpWindowsSymlinks->run();
